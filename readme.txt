=== BP Menus ===
Contributors: modemlooper
Tags: buddypress, social networking, activity, profiles, messaging, friends, groups, forums, microblogging, twitter, facebook, social, community, networks, networking, cms
Requires at least: 3.2
Tested up to: 3.4
Stable tag: 2.0.3
License: General Public License version 2


== Description ==
WARNING: You may need to redo menu items. If a menu item breaks remove it and add it back to your menu. 

This plugin will add a BuddyPress Menu to the admin at Appearance > Menus. You can re arrange the menu which includes the user profile links.


== Installation ==
1. Activate plugin.
2. Go to Appearance > Menus in the WordPress admin.
3. Add items to a menu
3. Re-arrange your menu. It's best to put all user links under "Profile" to create a drop down.




== Screenshots ==
1. BuddyPress Menu Meta Box in the admin


== Faq ==


	
== Changelog ==
= 2.0.3 =
fixed issue with cookie loading same content

= 2.0.2 =
Added more menu items

= 2.0 =
complete rewrite - adds meta box with addable items to any menu


== License ==

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See http://www.gnu.org/licenses/.